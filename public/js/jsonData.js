/*

  Формат JSON.

  JSON (JavaScript Object Notation)

  JSON.parse();
    Разбирает строку JSON, возможно с преобразованием получаемого значения и его свойств и возвращает разобранное значение.
    https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse
  JSON.stringify()
    Возвращает строку JSON, соответствующую указанному значени.
    https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
*/

var x = {
  name: 'asd',
  age: 25,
  doSmsng: function(){
    console.log('hello');
  }
};

var myJSON = '{"name":"asd"}';
var zz = JSON.parse(myJSON);
console.log(myJSON);

// var y = JSON.stringify(x);
// console.log( y );
// //
// var z = JSON.parse(y);
// console.log(z);
// z.name = "asdasd";
// console.log(z);

/*
  JSON является синтаксисом для сериализации объектов, массивов, чисел, строк логических значений и значения nul
  т.е JSON работает со следущими типами данных:
  - Object : {}
  - Array : []
  - Number : 12
  - String : ""
  - Boolen : true
  - null

  Особености:
  1) Передает только данные
  2) Именна свойств должны быть строками заключенными в двойные кавычки
*/

/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (5-6 разных полей), конвертирует их в json и выводит в консоль.
    2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary


*/
//
// var myForm = Array.from(document.forms[0].elements);
// var jsonObj = {};
// myForm.forEach( function(item){
//   if( item.type !== "submit"){
//     console.log(item.name, item.value, item.type);
//     jsonObj[item.name] = item.value;
//   }
//
// });
// console.log(jsonObj);
